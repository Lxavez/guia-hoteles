     $(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
   interval:2000
  });
  $('#contacto').on('show.bs.modal',function(e){
    console.log('El modal se esta mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled',true);
  });
})
